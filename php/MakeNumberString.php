<?php
/**
 * Created by PhpStorm.
 * User: artur
 * Date: 08.02.2018
 * Time: 19:05
 */

class MakeNumberString
{
    protected $nnFormat;

    public function __construct()
    {
        $this->nFormat = new NumberFormatter('pl', NumberFormatter::SPELLOUT);
        $this->nFormat->setTextAttribute(NumberFormatter::DEFAULT_RULESET, "%spellout-cardinal-masculine-accusative");
    }

    public function parseIt($number_string)
    {
        $number_string=str_replace(',','.',$number_string);
        if (is_numeric($number_string))
        {
            $int_strings = $this->getIntsStrings(round(floatval($number_string),2), FALSE);
            return $int_strings[0].' '. (isset($int_strings[1]) ? 'przecinek '.$int_strings[1] : '');
        }
        else
        {
            $temp = explode(' ', $number_string);
            if (count($temp) == 2)
            {
                $number = round(floatval($temp[0]),2);
                $currency = $temp[1];
                unset($temp);
                $int_strings = $this->getIntsStrings($number);
                $currency_map = $this->getCurrencyStringsMap($currency);
                if($currency_map)
                {
                    $currency_strings = $this->getCurrencyStrings($currency_map, $number);
                    if($int_strings && $currency_strings)
                    {
                        return $int_strings[0]. ' '.$currency_strings[0].' '.$int_strings[1]. ' '.$currency_strings[1];
                    }
                }
            }
        }
        return FALSE;
    }

    protected function getCurrencyStrings($currency_map, $number)
    {
        $temp = explode('.', $number);


        if(count($temp)<2)
        {
            $temp[1]=0;
        }
        return [
            $this->getValidStringFromMap($currency_map['first'], $temp[0]),
            $this->getValidStringFromMap($currency_map['second'], $temp[1])
        ];
    }

    protected function getValidStringFromMap($currency_map, $int)
    {
        $dec = 0;
        if(strlen($int) > 1)
        {
            $dec = substr($int, -2,1);
            $int = substr($int, -1);
        }
        if(array_key_exists($int, $currency_map) && $dec != 1)
        {
            return $currency_map[$int];
        }
        else
        {
            return $currency_map['rest'];
        }
    }

    /**
     * @param $currency_short
     * @return array|bool
     */
    public function getCurrencyStringsMap($currency_short)
    {
        $currencies=[];
        switch($currency_short){
            case 'zł':
                $currencies = [
                    'first' => [
                        1 => 'złoty',
                        2 => 'złote',
                        3 => 'złote',
                        4 => 'złote',
                        'rest' => 'złotych'
                    ],
                    'second' => [
                        1 => 'grosz',
                        2 => 'grosze',
                        3 => 'grosze',
                        4 => 'grosze',
                        'rest' => 'groszy',
                    ],
                ];
            break;
            case '€':
                $currencies = [
                    'first' => [
                        'rest' => 'euro'
                    ],
                    'second' => [
                        1 => 'eurocent',
                        2 => 'eurocenty',
                        3 => 'eurocenty',
                        4 => 'eurocenty',
                        'rest' => 'eurocentów',
                    ],
                ];
            break;
            case '$':
                $currencies = [
                    'first' => [
                        1 => 'dolar',
                        2 => 'dolary',
                        3 => 'dolary',
                        4 => 'dolary',
                        'rest' => 'dolarów',
                    ],
                    'second' => [
                        1 => 'cent',
                        2 => 'centy',
                        3 => 'centy',
                        4 => 'centy',
                        'rest' => 'centów',
                    ],
                ];
            break;
        }
        if($currencies)
        {
            return $currencies;
        }
        return FALSE;
    }

    /**
     * @param $var
     * @param $second bool
     * @return array|FALSE
     */
    public function getIntsStrings($var, $second=TRUE)
    {
        if (is_numeric($var))
        {
            if (is_float($var))
            {
                $temp = explode('.', $var);
                $return[0] = $this->intToString($temp[0]);
                if($second)
                {
                    if (count($temp) < 2)
                    {
                        $temp[1]=0;
                    }
                    $return[1] = $this->intToString($temp[1]);
                }
                else
                {
                    if(isset($temp[1]))
                    {
                        $return[1] = $this->intToString($temp[1]);
                    }
                }
            }
            else
            {
                $return = FALSE;
            }
        }
        else
        {
            $return = FALSE;
        }
        return $return;
    }

    /**
     * @param $int int
     * @return string
     */
    public function intToString($int)
    {
        return $this->nFormat->format($int);
    }
}