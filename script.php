<?php
/**
 * Created by PhpStorm.
 * User: artur
 * Date: 08.02.2018
 * Time: 18:15
 */
spl_autoload_register(function ($class) {
    include 'php/' . $class . '.php';
});
$request=json_decode(file_get_contents( 'php://input' ), TRUE);
if(isset($request['stringme']))
{
    $mns = new MakeNumberString;
    echo json_encode($mns->parseIt($request['stringme']));
}
else
{
    die('');
}